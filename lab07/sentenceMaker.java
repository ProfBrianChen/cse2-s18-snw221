import java.util.Scanner;
import java.util.Random;
public class sentenceMaker{
  public static String adjs (int number){
    String adj ="word";
    switch (number){
      case 0:
        adj= "brown";
        break;
      case 1:
        adj= "beautiful";
        break;
      case 2:
        adj= "energetic";
        break;
      case 3:
        adj= "positive";
        break;
      case 4:
        adj= "amazing";
        break;
      case 5:
        adj= "brave";
        break;
      case 6:
        adj= "gilded";
        break;
      case 7:
        adj= "sneaky";
        break;
      case 8:
        adj= "vain";
        break;
      case 9:
        adj= "witty";
        break;
    }
    return adj;
  }
  public static String subjectNoun (int number){ 
    String sNoun ="word";
    switch (number){
      case 0:
        sNoun= "dog";
        break;
      case 1:
        sNoun= "cat";
        break;
      case 2:
        sNoun= "kitten";
        break;
      case 3:
        sNoun= "girl";
        break;
      case 4:
        sNoun= "teacher";
        break;
      case 5:
        sNoun= "boy";
        break;
      case 6:
        sNoun= "puppy";
        break;
      case 7:
        sNoun= "professor";
        break;
      case 8:
        sNoun= "giraffe";
        break;
      case 9:
        sNoun= "panda"; 
        break;
    }
    return sNoun;
  }
  public static String  objectNoun (int number){ 
    String oNoun ="word";
    switch (number){
      case 0:
        oNoun= "pizza";
        break;
      case 1:
        oNoun= "pasta";
        break;
      case 2:
        oNoun= "chicken";
        break;
      case 3:
        oNoun= "pie";
        break;
      case 4:
        oNoun= "cake";
        break;
      case 5:
        oNoun= "icecream";
        break;
      case 6:
        oNoun= "salad";
        break;
      case 7:
        oNoun= "sandwich";
        break;
      case 8:
        oNoun= "burger";
        break;
      case 9:
        oNoun= "buritto";
        break;
    }
    return oNoun;
  }
  public static String pastVerb (int number){ 
    String pVerb ="word";
    switch (number){
      case 0:
        pVerb= "ate";
        break;
      case 1:
        pVerb= "chewed";
        break;
      case 2:
        pVerb= "bit";
        break;
      case 3:
        pVerb= "swallowed";
        break;
      case 4:
        pVerb ="guzzled";
        break;
      case 5:
        pVerb ="tasted";
        break;
      case 6:
        pVerb= "nibbled";
        break;
      case 7:
        pVerb= "gobbled";
        break;
      case 8:
        pVerb ="digested";
        break;
      case 9:
        pVerb ="devoured";
        break;
    }
    return pVerb;
  }
  public static void addSent(String first){
     Random randomGenerator = new Random();
      int randomInt = randomGenerator.nextInt(10);
      String pVerb = pastVerb(randomInt);
      randomInt = randomGenerator.nextInt(10);
      String adj = adjs(randomInt);
      randomInt = randomGenerator.nextInt(10);
      String oNoun = objectNoun(randomInt);
      randomInt = randomGenerator.nextInt(10);
      System.out.println("The "+first+" "+pVerb+" "+"the "+oNoun+"."); 
  }
  public static void conclusion(String first){
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10);
    String pVerb = pastVerb(randomInt);
    String adj = adjs(randomInt);
    String oNoun = objectNoun(randomInt);
    System.out.println("The "+first+" "+pVerb+" "+"the "+oNoun+"!");
  }
  public static void phase1(){
    String yesOrNo;
    boolean again;
    do{
      Random randomGenerator = new Random();
      int randomInt = randomGenerator.nextInt(10);
      String sNoun = subjectNoun(randomInt);
      randomInt = randomGenerator.nextInt(10);
      String pVerb = pastVerb(randomInt);
      randomInt = randomGenerator.nextInt(10);
      String adj = adjs(randomInt);
      randomInt = randomGenerator.nextInt(10);
      String oNoun = objectNoun(randomInt);
      randomInt = randomGenerator.nextInt(10);
      System.out.println("The "+adj+" "+sNoun+" "+pVerb+" "+"the "+oNoun+".");
      Scanner myScanner = new Scanner( System.in );
      System.out.print("Would you like another sentence? Answer y for yes or n for no : ");
      yesOrNo = myScanner.next();
      if (yesOrNo.equals("y")){
        again=true;
      }
      else { again = false; }
    }  
    while (again);
  
  }
  public static String story(){
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10);
    String sNoun = subjectNoun(randomInt);
    String firstNoun = sNoun;
    randomInt = randomGenerator.nextInt(10);
    String pVerb = pastVerb(randomInt);
    randomInt = randomGenerator.nextInt(10); 
    String adj = adjs(randomInt);
    randomInt = randomGenerator.nextInt(10);
    String oNoun = objectNoun(randomInt);
    randomInt = randomGenerator.nextInt(10);
    System.out.println("The "+adj+" "+sNoun+" "+pVerb+" "+"the "+oNoun+"."); 
    return firstNoun;
  }
  public static void main(String[] args){
    phase1();
    String firstNoun = story();
    addSent(firstNoun);
    conclusion(firstNoun);
    
  } 
}