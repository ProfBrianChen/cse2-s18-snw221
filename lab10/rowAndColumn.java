import java.util.Random;
public class rowAndColumn{
  public static int[][] increasingMatrix(int height, int width, boolean format){
    if (format){
      int [][] array = new int [height][width];
      int num = 1;
      for (int i = 0; i< height; i++){
        for (int j = 0; j<width; j++){
          array[i][j] =num;
          num++;
        } 
      }
      printMatrix(array,format);
      return array;
    }
    else {
     int [][] array = new int [width][height];
      int num1 = 1;
      for (int i = 0; i <height; i++){
        for (int j = 0; j<width; j++){
          array[j][i] =num1;
          num1++;
        }
      }
      printMatrix(array,format);
      return array;
    }
   
  }
  public static void printMatrix( int[][] array, boolean format ){
      for(int i=0; i<array.length;i++){
        for (int j=0; j<array[0].length;j++){
          System.out.print(array[i][j] + " ");
        }
        System.out.println("");
      }
      System.out.println("");
    }
  public static int [][] translate(int[][] array){
    int [][] newArray = new int [array[0].length][array.length];
    for(int i=0; i<array.length;i++){
      for (int j=0; j<array[0].length;j++){
          newArray[j][i] = array[i][j];
      }
    }
    return newArray;
  }
  public static void addMatrix( int[][] a, boolean formata, int[][] b, boolean formatb){
    if (!formata){
      a = translate(a);
    }
    if (!formatb){
      b = translate(b);
    }
    
    if(a.length == b.length && a[0].length == b[0].length){
      int [][] c = new int [b.length][b[0].length];
      for (int i =0; i<b.length; i++){
        for (int j =0; j<b[0].length; j++){
          c[i][j]= a[i][j] + b[i][j];
        }
      }
      printMatrix(c, true); 
    }
    else{
      System.out.println("Arrays cannot be added.");
//       return null;
      
    }
  }
  public static void main(String [] args){
    int height = (int) (Math.random()*10 +1);
    int width =(int) (Math.random()*10);
    int [][] A = increasingMatrix(height,width, true);
    int [][] B =increasingMatrix(height,width, false); 
    addMatrix(A, true, B, false);
    
  }
}
  

