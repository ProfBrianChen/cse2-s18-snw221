//////////////////////////////////////////
///cse 2 Welcome Class
///
public class WelcomeClass {
  
  public static void main(String[] args) {
    // Prints A Welcome Message to the terminal window.
    System.out.println("-----------");
    System.out.println("| WELCOME |");
    System.out.println("-----------");
    System.out.println(" ^  ^  ^  ^  ^  ^");
    System.out.println("/ \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("<-E--J--K--O--O--O->");
    System.out.println("\\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println(" v  v  v  v  v  v");
  }
  
}