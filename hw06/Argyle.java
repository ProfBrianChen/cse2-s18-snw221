import java.util.Scanner;
public class Argyle{
  public static void main(String[]args){
     Scanner myScanner = new Scanner (System.in);
    
    //width
     System.out.print("Enter a positive integer for the width of Viewing window in characters: ");
    while (!myScanner.hasNextInt()){
      String junkWord = myScanner.next();
      System.out.print("Invalid Input. Enter a positive integer for the width of Viewing window in characters: ");
    }
    int windowWidth = myScanner.nextInt();
    while (windowWidth < 0){
        System.out.print("Invalid Input. Enter a positive integer for the width of Viewing window in characters: ");
        while (!myScanner.hasNextInt()){
          String junkWord = myScanner.next();
          System.out.print("Invalid Input. Enter a positive integer for the width of Viewing window in characters: ");
        }
        windowWidth = myScanner.nextInt();
      }
    
    //height
    System.out.print("Enter a positive integer for the height of Viewing window in characters: ");
    while (!myScanner.hasNextInt()){
      String junkWord = myScanner.next();
      System.out.print("Invalid Input. Enter a positive integer for the height of Viewing window in characters: ");
    }
    int windowHeight = myScanner.nextInt();
    while (windowHeight < 0){
      System.out.print("Invalid Input. Enter a positive integer for the height of Viewing window in characters: ");
      while (!myScanner.hasNextInt()){
        String junkWord = myScanner.next();
        System.out.print("Invalid Input. Enter a positive integer for the height of Viewing window in characters: ");
      }
      windowHeight = myScanner.nextInt();
    }
    
    //patternWidth
     System.out.print("Enter a positive integer for the width of the argyle diamonds: ");
       while (!myScanner.hasNextInt()){
         String junkWord = myScanner.next();  
         System.out.print("Invalid Input. Enter a positive integer for the width of the argyle diamonds: ");
       }
       int patternWidth = myScanner.nextInt();
       while (patternWidth < 0){
         System.out.print("Invalid Input. Enter a positive integer for the width of the argyle diamonds: ");
         while (!myScanner.hasNextInt()){
           String junkWord = myScanner.next();
           System.out.print("Invalid Input. Enter a positive integer for the width of the argyle diamonds: ");
         }
         patternWidth = myScanner.nextInt();
       }
         
       //Stripe input
       System.out.print("Enter a positive odd integer for the width of the argyle center stripe: ");
         while (!myScanner.hasNextInt()){
           String junkword =myScanner.next();
           System.out.print("Invalid input. Enter a positive odd integer for the width of the argyle center stripe: ");
         }
       int stripeInput = myScanner.nextInt();
       while (stripeInput < 0 || stripeInput % 2 == 0){
         System.out.print("Invalid input. Enter a positive odd integer for the width of the argyle center stripe: ");
         while (!myScanner.hasNextInt()){
           String junkword =myScanner.next();
           System.out.print("Invalid input. Enter a positive odd integer for the width of the argyle center stripe: ");
         }
         stripeInput = myScanner.nextInt();
       } 
         
       //background char
       System.out.print("Enter a first character for the pattern fill: ");
       String holder = myScanner.next();
       char background = holder.charAt(0);
       
    
       //diamond char
       System.out.print("Enter a second character for the pattern fill: ");
       String place = myScanner.next();
       char diamond = place.charAt(0);
    
       //Stripe char
       System.out.print("Enter a third character for the stripe fill: ");
       String spot = myScanner.next();
       char stripe = spot.charAt(0);
         
       int stripeWidth = stripeInput/2;
    
       for(int i = 0; i < windowHeight; i++){
         int patternI = i % (patternWidth*2);
         if (patternI > patternWidth){
           patternI = ((patternWidth*2)-patternI);
         }
         int d = patternI*2;
         int width1 = ((patternWidth*2)-d)/2;
         int width2 = ((patternWidth*2)-d)/2;
         for (int j = 0; j < windowWidth; j++){
           if (d+width1+width2 == 0){
             d = patternI*2;
             width1 = ((patternWidth*2)-d)/2;
             width2 = ((patternWidth*2)-d)/2;
           }
           int patternJ =j;
           patternJ = patternJ%(patternWidth*2);
         
           if (patternJ<= patternI + stripeWidth && patternJ>= patternI - stripeWidth){
             System.out.print(stripe); 
             if(width1 > 0){
               width1--;
             }
             else if(d > 0){
               d--;
             }
             else if (width2 > 0){
               width2--;
             }
             continue;
           }
           else if (patternJ>=(patternWidth*2)-patternI-1-stripeWidth && patternJ<=(patternWidth*2)-patternI-1+stripeWidth){
             System.out.print(stripe); 
             if(width1 > 0){
               width1--;
             }
             else if(d > 0){
               d--;
             }
             else if (width2 > 0){
               width2--;
             }
             continue;
           }
           if(width1 > 0){
             System.out.print(background);
             width1--;
           }
           else if(d > 0){
             System.out.print(diamond);
             d--;
           }
           else if (width2 > 0){
             System.out.print(background);
             width2--;
           }
         }
         System.out.println("");
       }
    
  }
} 