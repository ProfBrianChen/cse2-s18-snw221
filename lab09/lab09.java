public class lab09{
  public static int [] copy(int [] array){
    int [] copiedArray = new int [array.length];
    for (int i = 0; i < array.length; i++){
      copiedArray[i]=array[i];
    }
    return copiedArray;
  }
  public static void inverter(int [] array){
    for (int i = 0; i < (array.length/2);i++){
      int temp= array[i];
      array[i] = array[array.length-i-1];
      array[array.length-i-1] = temp;
    }
  }
  public static int [] inverter2(int [] array){
    int [] newArray = copy (array);
    inverter(newArray);
    return newArray;
  }
  public static void print(int [] array){
    for (int i = 0; i<array.length; i++ ){
      System.out.print(array[i] + " ");
    }
    System.out.println("");
  }
  public static void main(String [] args){
    int [] array0 = new int [8];
    for (int i = 0; i < 8; i++){
      array0[i]= i+1;
    }
    int [] array1 = copy (array0);
    int [] array2 = copy (array0);
    inverter(array0);
    print(array0);
    inverter2(array1);
    print(array1);
    int [] array3 = inverter2(array2);
    print(array3);
    
  }
}