import java.util.Scanner;
public class StringAnalysis{
  public static boolean string(String word){
    for (int i = 0; i < word.length(); i++){
      if (word.charAt(i)>='a' && word.charAt(i)<='z'){
        
      }
      else{
        return false;
      }
    }
    return true;
    
  }
  
  
  public static boolean string(String word, int number){
    if(number> word.length()){
      number = word.length();
    } 
    for (int i = 0; i < number; i++){
      if (word.charAt(i)>='a' && word.charAt(i)<='z'){
        
      }
      else{
        return false;
      }
    }
    return true;
  }
  
  
  public static void main(String[] args){
    boolean answer;
    Scanner myScanner = new Scanner (System.in);
    System.out.print("Enter string: ");
    String input = myScanner.next();
//     int length = input.length();
    System.out.print("Would you like to examine the characters? Answer y for yes or n for no: ");
    String examine = myScanner.next();
    while (!examine.equals("y") && !examine.equals("n")){
      System.out.print("Invalid Input. Would you like to examine the characters? Answer y for yes or n for no: ");
      examine = myScanner.next();
    }
    if (examine.equals("y")){
      System.out.print("Do you want to check all the characters or just certain a number? Answer all or number: ");
      String allOrNumber =myScanner.next();
      while (!allOrNumber.equals("all") && !allOrNumber.equals("number")){
        System.out.print("Invalid Input. Do you want to check all the characters or just certain a number? Answer all or number: ");  
        allOrNumber = myScanner.next();
      }
      if (allOrNumber.equals("all")){
        answer = string(input);
      }
      else{
        System.out.print("How many characters would you like to check? ");
        while (!myScanner.hasNextInt()){
          String junkword = myScanner.next();
          System.out.print("Invalid Input. How many characters would you like to check? ");
        }
        int number = myScanner.nextInt();
        answer = string(input,number);
      }
    }
    else{
      return;
    }
    if (answer){
      System.out.println("The characters are all letters.");
    }
    else{
      System.out.println("The characters are not all letters.");
    }
  }
}