import java.util.Scanner;
public class Area{
  public static double check(){
    Scanner myScanner = new Scanner( System.in );
    System.out.print("Value of shape: ");
    while (!myScanner.hasNextDouble()){
      String junkWord = myScanner.next();
      System.out.print("Invalid input. Value of shape: ");
    }
    double value = myScanner.nextDouble();
    while (value < 0){
      System.out.print("Invalid input. Value of shape: ");
      while (!myScanner.hasNextDouble()){
        String junkWord = myScanner.next();
        System.out.print("Invalid input. Value of shape: ");
      }
      value = myScanner.nextDouble();
    }
    return value;
  }
  public static void triangleArea(){
    double width = check();
    double height = check();
    double area = height*width*.5;
    System.out.println("Area of triangle: "+ area);
  }
  
  
  public static void rectArea(){
    double width = check();
    double height = check();
    double area = height*width;
    System.out.println("Area of rectangle: "+ area);
  }
  
  
  public static void circleArea(){
    double radius = check();
    double area =radius*radius*3.14159265;
    System.out.println("Area of circle: "+ area);
  }
  
  
  public static void main(String[] args){
    Scanner myScanner = new Scanner( System.in );
    System.out.print("Choose from triangle, rectangle, or circle. Enter shape with no capitalization: ");
    String shape = myScanner.next();
    while (!shape.equals("triangle") && !shape.equals("rectangle") && !shape.equals("circle") ){
      System.out.print("Invalid input. Choose from triangle, rectangle, or circle. Enter shape with no capitalization: ");
      shape = myScanner.next();
     }
    if (shape.equals("triangle")){
      triangleArea();
      
    }
    else if (shape.equals("rectangle")){
      rectArea();
    }
    else if (shape.equals("circle")){
      circleArea();
    }
  }
  
}