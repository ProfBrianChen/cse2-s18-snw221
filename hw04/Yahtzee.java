import java.util.Scanner;

public class Yahtzee{
  
  public static void main(String[]args){
    Scanner myScanner = new Scanner( System.in );
    System.out.print("Roll randomly in the form of \"roll\" or enter a 5 digit number representing the result of a specific roll in the form of \"digits\": ");
    String rollOr5digit= myScanner.next();
    int die1 = 0;
    int die2 = 0;
    int die3 = 0; 
    int die4 = 0; 
    int die5 = 0;
    if ( rollOr5digit.equals("roll") ){
      die1 = (int)(Math.random()*(6))+1;
      System.out.println("Random Roll 1: "+ die1);
      die2 = (int)(Math.random()*(6))+1;
      System.out.println("Random Roll 2: "+ die2);
      die3 = (int)(Math.random()*(6))+1;
      System.out.println("Random Roll 3: "+ die3);
      die4 = (int)(Math.random()*(6))+1;
      System.out.println("Random Roll 4: "+ die4);
      die5 = (int)(Math.random()*(6))+1;
      System.out.println("Random Roll 5: "+ die5);
    }
    else if ( rollOr5digit.equals("digits") ){
      System.out.print("Enter 5 digits: ");
      String digitInput= myScanner.next();
      if ( digitInput.length() != 5 ){
        System.out.println("Invalid Input"); 
        return;
      }
      else if ((digitInput.charAt(0) <'1') || (digitInput.charAt(0) > '6')){
        System.out.println("Invalid Input"); 
        return;
      }
      else if ((digitInput.charAt(1) <'1') || (digitInput.charAt(1) > '6')){
        System.out.println("Invalid Input");
        return;
      } 
      else if ((digitInput.charAt(2) <'1') || (digitInput.charAt(2) > '6')){
         System.out.println("Invalid Input");
        return;
      } 
      else if ((digitInput.charAt(3) <'1') || (digitInput.charAt(3) > '6')){
         System.out.println("Invalid Input");
        return;
      } 
      else if ((digitInput.charAt(4) <'1') || (digitInput.charAt(4) > '6')){
         System.out.println("Invalid Input");
        return;
       }
      else {
        die1 = digitInput.charAt(0)-48;
        die2 = digitInput.charAt(1)-48;
        die3 = digitInput.charAt(2)-48;
        die4 = digitInput.charAt(3)-48;
        die5 = digitInput.charAt(4)-48;
        
      }
    }
    int aces = 0;
    int twos = 0;
    int threes = 0;
    int fours = 0;
    int fives = 0;
    int sixes = 0;
    switch (die1){
      case 1:
      aces += die1;
        break;
      case 2:
      twos += die1;
        break;
      case 3:
      threes += die1;
        break;
      case 4:
      fours += die1;
        break;
      case 5:
      fives += die1;
        break;
      case 6:
        sixes += die1;
        break;
    }
    switch (die2){
      case 1:
      aces += die2;
        break;
      case 2:
      twos += die2;
        break;
      case 3:
      threes += die2;
        break;
      case 4:
      fours += die2;
        break;
      case 5:
      fives += die2;
        break;
      case 6:
        sixes += die2;
        break;
  }
  switch (die3){
      case 1:
      aces += die3;
        break;
      case 2:
      twos += die3;
        break;
      case 3:
      threes += die3;
        break;
      case 4:
      fours += die3;
        break;
      case 5:
      fives += die3;
        break;
      case 6:
        sixes += die3;
        break;
    }
    switch (die4){
      case 1:
      aces += die4;
        break;
      case 2:
      twos += die4;
        break;
      case 3:
      threes += die4;
        break;
      case 4:
      fours += die4;
        break;
      case 5:
      fives += die4;
        break;
      case 6:
        sixes += die4;
        break;
    }
   switch (die5){
      case 1:
      aces += die5;
        break;
      case 2:
      twos += die5;
        break;
      case 3:
      threes += die5;
        break;
      case 4:
      fours += die5;
        break;
      case 5:
      fives += die5;
        break;
      case 6:
        sixes += die5;
        break;
    }
    System.out.println(" ");
    System.out.println("Aces: "+ aces);
    System.out.println("Twos: "+ twos);
    System.out.println("Threes: "+ threes);
    System.out.println("Fours: "+ fours);
    System.out.println("Fives: "+fives);
    System.out.println("Sixes: "+sixes);
    int upSecTot = aces+twos+threes+fours+fives+sixes;
    if (upSecTot >65){
     upSecTot += 35;
    }
    System.out.println("Upper Section Total: " + upSecTot);
    System.out.println(" ");
    int threeOfKind = 0;
    if (aces ==3){
      threeOfKind = upSecTot;
    }
    else if (twos==6){
      threeOfKind = upSecTot;
    }
    else if (threes == 9){
      threeOfKind = upSecTot;
    }
    else if (fours == 12){
      threeOfKind = upSecTot;
    }
    else if (fives == 15){
      threeOfKind = upSecTot;
    }
    else if (sixes == 18){
      threeOfKind = upSecTot;
    }
    System.out.println("Three of a Kind Score: " + threeOfKind);
    int fourOfKind = 0;
    if (aces == 4){
      fourOfKind = upSecTot;
    }
    else if (twos == 8){
      fourOfKind = upSecTot;
    }
    else if (threes == 12){
      fourOfKind = upSecTot;
    }
    else if (fours == 16){
      fourOfKind = upSecTot;
    }
    else if (fives == 20){
      fourOfKind = upSecTot;
    }
    else if (sixes == 24){
      fourOfKind = upSecTot;
    }
    System.out.println("Four of a Kind Score: "+ fourOfKind);
    int yahtzee = 0;
    if (aces == 5){
      yahtzee = 50;
    }
    else if (twos == 10){
      yahtzee = 50;
    }
    else if (threes == 15){
      yahtzee = 50;
    }
    else if (fours == 20){
      yahtzee = 50;
    }
    else if (fives == 25){
      yahtzee = 50;
    }
    else if (sixes == 30){
      yahtzee = 50;
    }
    System.out.println("Yahtzee Score: "+ yahtzee);
    int fullHouse = 0;
    if(threeOfKind != 0 && (aces ==2 || twos == 4 || threes == 6 || fours == 8 || fives == 10 || sixes == 12)){
      fullHouse = 25;
    }
    System.out.println("Full House Score: "+ fullHouse);
    int lgStraight = 0;
    if ((twos==2 && threes==3 && fours==4 && fives ==5) && (aces==1 || sixes ==6)){
      lgStraight = 40;
    }
    System.out.println("Large Straight Score: " + lgStraight);
    int smStraight = 0;
    if ((threes==3 && fours==4) && ((aces==1 && twos==2) || (twos==2 && fives==5) || (fives==5 && sixes==6))){
      smStraight = 30; 
    }
    System.out.println("Small Straight Score: " + smStraight);
    System.out.println("Chance: " + upSecTot);
  }
}