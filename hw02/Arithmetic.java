//////////////////////////////////////////
///cse 2 Arithmetic
///
public class Arithmetic {
  
  public static void main(String[] args) {
    // Prints calculate price of something including tax to the terminal window.
    //Number of pairs of pants
int numPants = 3;
//Cost per pair of pants
double pantsPrice = 34.98;

//Number of shirts
int numShirts = 2;
//Cost per shirt
double shirtPrice = 24.99;

//Number of belts
int numBelts = 1;
//cost per belts
double beltCost = 33.99;

//the tax rate
double paSalesTax = 0.06;
    // Things I am calculating
    double TotalPricePants, TotalPriceShirts, TotalPriceBelts, TotalSalesTax, TotalPaid, SalesTaxPants, SalesTaxShirts, CostofItems, SalesTaxBelts;
    TotalPricePants = numPants*pantsPrice;
    SalesTaxPants = numPants*paSalesTax;
    TotalPriceShirts = numShirts*shirtPrice;
    SalesTaxShirts = paSalesTax*TotalPriceShirts;
    TotalPriceBelts = numBelts*beltCost;
    SalesTaxBelts = TotalPriceBelts*paSalesTax;
    TotalSalesTax = SalesTaxShirts+SalesTaxBelts+SalesTaxPants;
    CostofItems = TotalPriceBelts+TotalPriceShirts+TotalPricePants;
    TotalPaid = TotalSalesTax+TotalPriceBelts+TotalPricePants+TotalPriceShirts;
    
    TotalSalesTax *= 100; 
    int intTotalSalesTax = (int) TotalSalesTax;
    TotalSalesTax = intTotalSalesTax/100.0;
    
    TotalPaid *= 100;
    int intTotalPaid = (int) TotalPaid;
      TotalPaid = intTotalPaid/100.0;
    

    System.out.println("The price of all the items before tax is $"+CostofItems+".");
    System.out.println("The total sales tax is $"+TotalSalesTax+".");
    System.out.println("The total cost of purchases including tax is $"+TotalPaid+".");
   }
  
}