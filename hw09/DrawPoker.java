import java.util.Random;
public class DrawPoker{
  public static String[] shuffle(String[] deck) {
    Random rgen = new Random();
    for (int i = 0; i < deck.length; i++) {
        int randPos = rgen.nextInt(deck.length);
        String tmp = deck[i];
        deck[i] = deck[randPos];
        deck[randPos] = tmp;
    }
    return deck;
  }
  
  public static boolean pair(int[] array){
    for (int i = 0; i< array.length; i++){
      for (int j = 0; j<array.length; j++){
        if (i == j){
          continue;
        }
        if (array[j]==array[i]){
          return true;
        }
      }
    }
    return false;
  }
  public static boolean ThreeofaKind(int[] array){
    for (int i = 0; i< array.length; i++){
      for (int j = 0; j<array.length; j++){
        for (int k = 0; k <array.length; k++){
          if (i == j || i==k || k==j){
            continue;
          }
          if (array[j]==array[i] && array[j]==array[k]){
            return true;
          }
        }
      }
    }
    return false;
  }
  
  public static void print(String [] array){
    for (int i = 0; i<array.length; i++ ){
      System.out.println(array[i]);
    }
    System.out.println();
  }
  public static void printNum(int [] array){
    for (int i = 0; i<array.length; i++ ){
      System.out.println(array[i]);
    }
    System.out.println();
  }

  public static void main(String []args){
    String [] cards = new String[52];
    for (int i = 0; i<52; i++){
      switch ((int)(i%13)){
        case 0:
          cards[i] = "Ace";
          break;
        case 1:
          cards[i] = "2";
          break;
        case 2:
          cards[i] = "3";
          break;
        case 3:
          cards[i] = "4";
          break;
        case 4:
          cards[i] = "5";
          break;
        case 5:
          cards[i] = "6";
          break;
        case 6:
          cards[i] = "7";
          break;
        case 7:
          cards[i] = "8";
          break;
        case 8:
          cards[i] = "9";
          break;
        case 9:
          cards[i] = "10";
          break;
        case 10:
          cards[i] = "Jack";
          break;
        case 11:
          cards[i] = "Queen";
          break;
        case 12:
          cards[i] = "King";
          break;
      }
        
      
    }
    for (int i=0; i<52; i++){
      if ((int)(i/13)==0){
        cards[i] += " of Diamonds";
      }
      else if((int)(i/13)==1){
        cards[i] += " of Clubs";
      }
      else if ((int)(i/13)==2){
        cards[i] += " of Hearts";
      }
      else if ((int)(i/13)==3){
        cards[i] += " of Spades";
      }
    }
    
    String[] shuffledDeck = shuffle(cards);
    String[] player1 = new String[5];
    String[] player2 = new String[5];
    int j = 0;
    for (int i=0; i<10; i++){
        if (i%2 ==0){
          player1[j] = shuffledDeck[i];
        }
        else if (i%2==1){
          player2[j] = shuffledDeck[i];
          j++;
        }
     }
    System.out.println("Player 1's Hand:");
    print(player1);
    System.out.println("Player 2's Hand:");
    print(player2);
    int [] player1Num = new int [5];
    int [] player2Num = new int [5];
    for (int i = 0; i<5; i++){
      if (player1[i].charAt(0)==('A')){
        player1Num[i] = 1;
      }
      else if (player1[i].charAt(0)==('2')){
        player1Num[i] = 2;
      }
      else if (player1[i].charAt(0)==('3')){
        player1Num[i] = 3;
      }
      else if (player1[i].charAt(0)==('4')){
        player1Num[i] = 4;
      }
      else if (player1[i].charAt(0)==('5')){
        player1Num[i] = 5;
      }
      else if (player1[i].charAt(0)==('6')){
        player1Num[i] = 6;
      }
      else if (player1[i].charAt(0)==('7')){
        player1Num[i] = 7;
      }
      else if (player1[i].charAt(0)==('8')){
        player1Num[i] = 8;
      }
      else if (player1[i].charAt(0)==('9')){
        player1Num[i] = 9;
      }
      else if (player1[i].charAt(0)==('1')){
        player1Num[i] = 10;
      }
      else if (player1[i].charAt(0)==('J')){
        player1Num[i] = 11;
      }
      else if (player1[i].charAt(0)==('Q')){
        player1Num[i] = 12;
      }
      else if (player1[i].charAt(0)==('K')){
        player1Num[i] = 13;
      }
    }
    
    for (int i = 0; i<5; i++){
      if (player2[i].charAt(0)==('A')){
        player2Num[i] = 1;
      }
      else if (player2[i].charAt(0)==('2')){
        player2Num[i] = 2;
      }
      else if (player2[i].charAt(0)==('3')){
        player2Num[i] = 3;
      }
      else if (player2[i].charAt(0)==('4')){
        player2Num[i] = 4;
      }
      else if (player2[i].charAt(0)==('5')){
        player2Num[i] = 5;
      }
      else if (player2[i].charAt(0)==('6')){
        player2Num[i] = 6;
      }
      else if (player2[i].charAt(0)==('7')){
        player2Num[i] = 7;
      }
      else if (player2[i].charAt(0)==('8')){
        player2Num[i] = 8;
      }
      else if (player2[i].charAt(0)==('9')){
        player2Num[i] = 9;
      }
      else if (player2[i].charAt(0)==('1')){
        player2Num[i] = 10;
      }
      else if (player2[i].charAt(0)==('J')){
        player2Num[i] = 11;
      }
      else if (player2[i].charAt(0)==('Q')){
        player2Num[i] = 12;
      }
      else if (player2[i].charAt(0)==('K')){
        player2Num[i] = 13;
      }
    }
    
    boolean pairPlayer1 = pair(player1Num);
    boolean pairPlayer2 = pair(player2Num);
    if (pairPlayer1){
      System.out.println("Player One has a pair.");
    }
    else{
      System.out.println("Player One does not have a pair.");
    }
    if (pairPlayer2){
      System.out.println("Player Two has a pair.");
    }
    else{
      System.out.println("Player Two does not have a pair.");
    }
    boolean threePlayer1 = ThreeofaKind(player1Num);
    boolean threePlayer2 = ThreeofaKind(player2Num);
    if (threePlayer1){
      System.out.println("Player One has three of a kind.");
    }
    else{
      System.out.println("Player One does not have a three of a kind.");
    }
    if (threePlayer2){
      System.out.println("Player Two has a three of a kind.");
    }
    else{
      System.out.println("Player Two does not have a three of a kind.");
    }
  }
}