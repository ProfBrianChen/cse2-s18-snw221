import java.util.Random;
public class RobotCity{
  public static void display(int[][] cityArray){
    for(int i =0; i <cityArray.length; i++){
      for (int j = 0; j<cityArray[0].length; j++){
        System.out.printf("% 4d ", cityArray[i][j]);
      }
      System.out.println("");
    }
    System.out.println();
  }
  
  public static void update(int [][] cityArray){
    for (int i = 0; i<cityArray.length; i++){
      for (int j = 0; j<(cityArray[0].length-1); j++){
        if (cityArray[i][j]<0){
          cityArray[i][j+1] *= -1;
          cityArray[i][j] *= -1;
          j++;
        }
      }
    }
    display(cityArray);
  }
  public static void invade(int [][] cityArray, int k){
    for(int i=0; i<k; i++){
      int x, y;
      do{
        x = (int)(Math.random()*cityArray.length);
        y = (int)(Math.random()*cityArray[0].length);
      }while(cityArray[x][y]<0);
      cityArray[x][y] *= -1;
    }
    display(cityArray);
    for (int i =0; i<5; i++){
      update(cityArray);
    }
  }
  
  public static void buildCity(){
    int [][] cityArray = new int [(int)(((Math.random()*5))+10)][(int)(((Math.random()*5))+10)];
    for(int i =0; i <cityArray.length; i++){
      for (int j = 0; j<cityArray[0].length; j++){
        cityArray[i][j] = (int) ((Math.random()*899)+100);
        
      }
    }
    display(cityArray);
    invade(cityArray, (int)(Math.random()*10));
  }
  public static void main(String []arg){
    buildCity();
    
  }
}
