//Generate a random card to practice a magic trick
public class CardGenerator {
  
  public static void main(String[] args){
    String suit = ""; 
    String number = "";
    int random = (int)(Math.random()*(52))+1;
   if ( 1<=random && random<=13 ){
     suit = "Diamonds ";
     
   }
   else if ( 14<=random && random<=26 ){
     suit = "Clubs ";
    
   }
    else if ( 27<=random && random<=39 ){
      suit = "Hearts ";
   }
    else if ( 40<=random && random<=52 ){
      suit = "Spades ";
   }
    int identity = random%13;
      if ( identity == 0 ){
       number = "Ace";
       }
      else if ( identity == 1 ){
       number = "2";
       }
    else if ( identity == 2 ){
       number = "3";
       }
    else if ( identity == 3 ){
       number = "4";
       }
    else if ( identity == 4 ){
       number = "5";
       }
    else if ( identity == 5 ){
       number = "6";
       }
    else if ( identity == 6 ){
       number = "7";
       }
    else if ( identity == 7 ){
       number = "8";
       }
    else if ( identity == 8 ){
       number = "9";
       }
    else if ( identity == 9 ){
       number = "10";
       }
    else if ( identity == 10 ){
       number = "Jack";
       }
    else if ( identity == 11 ){
       number = "Queen";
       }
    else if ( identity == 12 ){
       number = "King";
       }
    System.out.println(number+" of "+suit);
  }
}