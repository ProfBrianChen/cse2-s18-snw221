import java.util.Scanner;
// Document your program. Place your comments here!
//
//
public class Pyramid{
    			// main method required for every Java program
   			public static void main(String[] args) {
          Scanner myScanner = new Scanner( System.in );
          System.out.print("The square side of the pyramid is (input length): ");
          double squareSide = myScanner.nextDouble();
          System.out.print("The height of the pyramid is (input height): ");
          double height = myScanner.nextDouble();
          double numerator, volume;
          numerator = squareSide*squareSide*height;
            volume = numerator/3;
          System.out.println("The volume inside the pyramid is: "+volume);
        }
}