import java.util.Scanner;
// Document your program. Place your comments here!
//
//
public class Convert {
  // main method required for every Java program
  public static void main(String[] args) {
    Scanner myScanner = new Scanner( System.in );
    System.out.print("Enter the affected area in acres: ");
    double affectedArea = myScanner.nextDouble();
    System.out.print("Enter the rainfall in the affected area: " );
    double rainfall = myScanner.nextDouble();
    affectedArea *= 0.0015625;
    rainfall *= 0.0000157828;
    System.out.println(affectedArea+" "+rainfall);
    double cubicMiles = rainfall*affectedArea;
    System.out.println(cubicMiles +" cubic miles");
  }
}