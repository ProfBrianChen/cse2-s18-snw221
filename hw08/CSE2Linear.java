import java.util.Scanner;
public class CSE2Linear{
  public static void main(String []args){
    Scanner myScanner = new Scanner(System.in);
    int [] finalGrades = new int [15];
    for (int i = 0; i<15; i++){
      System.out.print("Enter final grade: ");
      while (!myScanner.hasNextInt()){
        String junkWord = myScanner.next();
        System.out.print("Invaild answer. Enter final grade: ");
      }
      finalGrades[i]= myScanner.nextInt();
      while (finalGrades[i] < 0 || finalGrades[i] > 100){
        System.out.print("Invaild answer. Enter final grade: ");
        finalGrades[i]= myScanner.nextInt();
      }
      if (i>0){
        while (finalGrades[i-1] > finalGrades[i]){
          System.out.print("Invaild answer. Enter final grade: ");
          finalGrades[i]= myScanner.nextInt();
        }
      }
    }
    System.out.println("Final Grades: ");
    for (int j = 0; j<15; j++){
      System.out.print(finalGrades[j]+" ");
    }
    System.out.print("Search for a grade: ");
    int search = myScanner.nextInt();
    int low = 0;
    int high = finalGrades.length - 1;
    while (low<=high){
      int mid = (low+high)/2;
      if (search < finalGrades[mid]){
        high = mid-1;
      }
      else if(search == finalGrades[mid]){
        System.out.println("The grade was found.");
      }
      else{
        low = mid+1;
      }
    }
    System.out.println("The grade was not found.");
  }
}